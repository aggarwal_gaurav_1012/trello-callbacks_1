/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based 
    on the listID that is passed to it from the given data in cards.json. Then pass control back 
    to the code that called it by using a callback function.
*/

const data = require('./cards.json');

function getListsByCardsId(cardsId, callback) {
    setTimeout(() => {
        try {
            const lists = data[cardsId];
            callback(lists);
        } catch (error) {
            console.log("Error in reading data");
            callback([]);
        }
    }, 2000); 
}

module.exports = getListsByCardsId;
/* 
	Problem 5: Write a function that will use the previously written functions to get the following
    information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const getBoardById = require('./callback1.cjs');
const getListsByBoardId = require('./callback2.cjs');
const getListsByCardsId = require('./callback3.cjs');

function getThanosBoardInfo() {

    setTimeout(() => {
        
        // Get information from the Thanos boards
        getBoardById('mcu453ed', (board) => {
            console.log("Thanos Board Information:", board);

            // Get all the lists for the Thanos board
            getListsByBoardId('mcu453ed', (lists) => {
                console.log("\nAll Lists for Thanos Board:", lists);

                // Get all cards for the Mind list simultaneously
                const mindList = lists.find(list => list.name === 'Mind');
                if (mindList) {
                    getListsByCardsId(mindList.id, (cards) => {
                        console.log("\nAll Cards for the Mind List:", cards);

                        // Get all cards for the Mind list simultaneously
                        const spaceList = lists.find(list => list.name === 'Space');
                        if(spaceList) {
                            getListsByCardsId(spaceList.id, (cards) => {
                                console.log("\nAll Cards for the Space List:", cards);
                            })
                        } else {
                            console.log("\nSpace list not found on Thanos board.");
                        }
                    });
                } else {
                    console.log("\nMind list not found on Thanos board.");
                }
            });
        });
    }, 2000);
}

getThanosBoardInfo();
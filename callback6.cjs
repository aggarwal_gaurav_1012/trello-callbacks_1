/* 
    Problem 6: Write a function that will use the previously written functions to get the following
    information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const getBoardById = require('./callback1.cjs');
const getListsByBoardId = require('./callback2.cjs');
const getListsByCardsId = require('./callback3.cjs');

function getThanosBoardInfo() {
    setTimeout(() => {

        // Get information from the Thanos boards
        getBoardById('mcu453ed', (board) => {
            console.log("Thanos Board Information:", board);

            // Get all the lists for the Thanos board
            getListsByBoardId('mcu453ed', (lists) => {
                console.log("\nAll Lists for Thanos Board:", lists);

                // Get all cards for all lists simultaneously
                for (let index = 0; index < lists.length; index++) {
                    const list = lists[index];

                    getListsByCardsId(list.id, (cards) => {
                        if (!cards) {
                            console.log(`\nAll Cards for ${list.name} List: No cards for this list`);
                        } else {
                            console.log(`\nAll Cards for ${list.name} List:`, cards);
                        }
                    });
                }
            });
        });
    }, 2000);
}

getThanosBoardInfo();
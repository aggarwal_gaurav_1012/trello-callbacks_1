/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID 
    that is passed to it from the given data in lists.json. Then pass control back to the code that 
    called it by using a callback function.
*/

const data = require('./lists_1.json');

function getListsByBoardId(boardId, callback) {
    setTimeout(() => {
        try {
            const lists = data[boardId];
            callback(lists);
        } catch (error) {
            console.log("Error in reading data");
            callback([]);
        }
    }, 2000); 
}

module.exports = getListsByBoardId;
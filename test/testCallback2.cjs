const getListsByBoardId = require('../callback2.cjs');

// Passing one boardId from the data
const boardId = 'mcu453ed';

getListsByBoardId(boardId, (lists) => {
    console.log("Details belonging to that particular id:", lists);
});
const getBoardById = require('../callback1.cjs');

// Passing one boardId from the data
const boardId = "mcu453ed";

getBoardById(boardId, (board) => {
    if (board) {
        console.log("Board details:", board);
    } else {
        console.log("Board not found.");
    }
});